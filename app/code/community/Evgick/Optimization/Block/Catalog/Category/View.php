<?php

class Evgick_Optimization_Block_Catalog_Category_View extends Mage_Catalog_Block_Category_View
{
    private $currentCountry = null;

    public function _construct()
    {
        if (Mage::helper('evgick_optimization')->cacheCategoryListingDetails()) {
            $this->addData(array(
                'cache_lifetime' => false,
            ));
        }
    }

    public function getCacheTags()
    {
        if (Mage::helper('evgick_optimization')->cacheCategoryListingDetails()) {
            $tags = array_merge(parent::getCacheTags(), $this->getCurrentCategory()->getCacheIdTags());
            $tags[] = 'catalog_category';
            return $tags;
        } else {
            return parent::getCacheTags();
        }
    }

    public function getCacheKeyInfo()
    {
        if (Mage::helper('evgick_optimization')->cacheCategoryListingDetails()) {
            $cacheInfo = array(
                'BLOCK_TPL',
                Mage::app()->getStore()->getCode(),
                $this->getTemplateFile(),
                'template' => $this->getTemplate(),
                'country_id' => $this->getCurrentCountry(),
                'currency' => Mage::app()->getStore()->getCurrentCurrencyCode(),
                'secure' => Mage::app()->getStore()->isCurrentlySecure(),
                'url' => Mage::helper('core/url')->getCurrentUrl(),

            );

            if ($customer = Mage::getSingleton('customer/session')->getCustomer()) {
                $cacheInfo['customer_group'] = $customer->getGroupId();
            }

            return $cacheInfo;
        } else {
            return parent::getCacheKeyInfo();
        }
    }

    private function getCurrentCountry()
    {
        if (is_null($this->currentCountry)) {
            $countryId = Mage::getSingleton('checkout/cart')->getQuote()
                ->getShippingAddress()
                ->getCountryId();

            if (!$countryId) {
                if ($customer = Mage::getSingleton('customer/session')->getCustomer()) {
                    if ($address = $customer->getDefaultShippingAddress()) {
                        $countryId = $address->getCountryId();
                    }
                }
            }
            if (!$countryId) {
                $store = Mage::app()->getStore()->getId();
                $countryId = Mage::getStoreConfig(
                    Mage_Tax_Model_Config::CONFIG_XML_PATH_DEFAULT_COUNTRY,
                    $store);
            }
            $this->currentCountry = $countryId;
        }
        return $this->currentCountry;
    }
}