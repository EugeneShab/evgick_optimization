<?php

class Evgick_Optimization_Model_Observer extends Mage_Catalog_Model_Observer
{
    public function productItemStockSaveAfter(Varien_Event_Observer $observer)
    {
        if (Mage::helper('evgick_optimization')->cacheProductDetails() || Mage::helper('evgick_optimization')->cacheCategoryListingDetails()) {
            $stockItem = $observer->getEvent()->getItem();
            if ($stockItem->dataHasChangedFor('is_in_stock')) {
                if ($product = Mage::getModel('catalog/product')->load($stockItem->getProductId())) {
                    if ($tags = $this->getProductCacheTags($product)) {
                        Mage::app()->cleanCache($tags);
                    }
                }
            }
        }
    }

    public function productSaveAfter(Varien_Event_Observer $observer)
    {

        if (Mage::helper('evgick_optimization')->cacheProductDetails() || Mage::helper('evgick_optimization')->cacheCategoryListingDetails()) {
            $product = $observer->getEvent()->getProduct();
            //toDo clear cache of categories, that was unchecked
            if ($product->dataHasChangedFor('status') || $product->dataHasChangedFor('visibility')) {
                if ($tags = $this->getProductCacheTags($product)) {
                    Mage::app()->cleanCache($tags);
                }
            }
        }
    }

    private function getProductCacheTags(Mage_Catalog_Model_Product $product, $cleanSelf = true)
    {

        $tags = array();

        if ($cleanSelf) {
            if (in_array($product->getVisibility(), array(2, 3, 4))) {
                $tags['catalog_product_' . $product->getId()] = 'catalog_product_' . $product->getId();
                if ($categories = $product->getCategoryIds()) {
                    foreach ($categories as $_categoryId) {
                        $category = Mage::getModel('catalog/category')->load($_categoryId);
                        $tags = array_merge($tags, $this->getCategoryCacheTags($category));
                    }
                }
            }
        }

        //todo need disable this in config
        if ($product->getTypeId() == 'simple') {
            $parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')
                ->getParentIdsByChild($product->getId());

            if (count($parentIds)) {
                foreach ($parentIds as $parentProductId) {
                    if ($parentProduct = Mage::getModel('catalog/product')->load($parentProductId)) {
                        $tags['catalog_product_' . $parentProduct->getId()] = 'catalog_product_' . $parentProduct->getId();
                        if ($categories = $parentProduct->getCategoryIds()) {
                            foreach ($categories as $_categoryId) {
                                $category = Mage::getModel('catalog/category')->load($_categoryId);
                                $tags = array_merge($tags, $this->getCategoryCacheTags($category));
                            }
                        }
                    }
                }
            }
        }
        return ((count($tags)) ? array_unique($tags) : null);
    }

    private function getCategoryCacheTags(Mage_Catalog_Model_Category $category)
    {
        $tags = array();
        if ($category->getId()) {
            $tags['catalog_category_' . $category->getId()] = 'catalog_category_' . $category->getId();
            if ($parentIds = $category->getParentIds()) {
                foreach ($parentIds as $parentId) {
                    $tags['catalog_category_' . $parentId] = 'catalog_category_' . $parentId;
                }
            }
        }

        return $tags;
    }
}
