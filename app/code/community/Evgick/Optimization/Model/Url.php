<?php

/**
 * Url.php
 *
 * @category:   Evgick_Optimization
 * @apckage:    Evgick
 * @author:     Evgick <>
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Evgick_Optimization_Model_Url extends Mage_Catalog_Model_Url
{
    public function refreshProductRewrites($storeId)
    {
        $this->_categories = array();
        $storeRootCategoryId = $this->getStores($storeId)->getRootCategoryId();
        $this->_categories[$storeRootCategoryId] = $this->getResource()->getCategory($storeRootCategoryId, $storeId);

        $lastEntityId = 0;
        $process = true;

        /** @var $_hlp Evgick_Optimization_Helper_Data */
        $_hlp = Mage::helper('evgick_optimization');
        $optimizeUrlRewrite = $_hlp->optimizeUrlRewrite();

        $useCategoriesInUrl = Mage::getStoreConfig('catalog/seo/product_use_categories');

        while ($process == true) {
            $products = $this->getResource()->getProductsByStore($storeId, $lastEntityId);
            if (!$products) {
                $process = false;
                break;
            }

            $this->_rewrites = array();
            $this->_rewrites = $this->getResource()->prepareRewrites($storeId, false, array_keys($products));

            $loadCategories = array();
            foreach ($products as $product) {
                foreach ($product->getCategoryIds() as $categoryId) {
                    if (!isset($this->_categories[$categoryId])) {
                        $loadCategories[$categoryId] = $categoryId;
                    }
                }
            }

            if ($loadCategories) {
                foreach ($this->getResource()->getCategories($loadCategories, $storeId) as $category) {
                    $this->_categories[$category->getId()] = $category;
                }
            }

            foreach ($products as $product) {
                if ($optimizeUrlRewrite &&
                    (
                        $product->getData("status") == Mage_Catalog_Model_Product_Status::STATUS_DISABLED
                        ||
                        $product->getData("visibility") == Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE
                    )
                ) {
                    continue;
                }

                $this->_refreshProductRewrite($product, $this->_categories[$storeRootCategoryId]);

                if ($useCategoriesInUrl != "0" || !$optimizeUrlRewrite) {
                    foreach ($product->getCategoryIds() as $categoryId) {
                        if ($categoryId != $storeRootCategoryId && isset($this->_categories[$categoryId])) {
                            $this->_refreshProductRewrite($product, $this->_categories[$categoryId]);
                        }
                    }
                }
            }

            unset($products);
            $this->_rewrites = array();
        }

        $this->_categories = array();

        return $this;
    }
}
