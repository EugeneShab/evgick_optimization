<?php

/**
 * ${FILE_NAME}
 *
 * @category:   Evgick_Optimization
 * @apckage:    Evgick
 * @author:     Evgick <>
 */
class Evgick_Optimization_Model_Layout_Update extends Mage_Core_Model_Layout_Update
{
    public function fetchDbLayoutUpdates($handle)
    {
        /** @var $_hlp Evgick_Optimization_Helper_Data */
        $_hlp = Mage::helper('evgick_optimization');
        if (!$_hlp->rewriteLayoutUpdate()) {
            return parent::fetchDbLayoutUpdates($handle);
        }

        return false;
    }
}
