<?php

/**
 * Table.php
 *
 * @category:   Evgick_Optimization
 * @apckage:    Evgick
 * @author:     Evgick <>
 */
class Evgick_Optimization_Model_Entity_Attribute_Source_Table extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    static protected $_optionsDefaultCache = array();
    static protected $_optionsCache = array();

    public function preloadOptions($entityType, $attributes, $storeId)
    {
        $attributesIds = array();
        foreach ($attributes as $attribute) {
            $attributesIds[] = Mage::getSingleton('eav/config')->getAttribute($entityType, $attribute)->getId();
        }

        $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
            ->setPositionOrder('asc')
            ->setAttributeFilter(array('in' => $attributesIds))
            ->setStoreFilter($storeId)
            ->load();

        $options = $collection->toOptionArray();
        $optionsDefault = $collection->toOptionArray('default_value');
        $optionsAttribute = $collection->toOptionArray('attribute_id');

        $optionsTmp = array();
        $optionsDefaultTmp = array();
        foreach ($options as $option) {
            $optionsTmp[$option['value']] = $option;
        }

        foreach ($optionsDefault as $option) {
            $optionsDefaultTmp[$option['value']] = $option;
        }

        if (!isset(self::$_optionsCache[$storeId])) {
            self::$_optionsCache[$storeId] = array();
        }

        foreach ($optionsAttribute as $_optionsAttribute) {
            if (!isset(self::$_optionsCache[$storeId][$_optionsAttribute['label']])) {
                self::$_optionsCache[$storeId][$_optionsAttribute['label']] = array();
            }
            self::$_optionsCache[$storeId][$_optionsAttribute['label']][] = $optionsTmp[$_optionsAttribute['value']];
            if (!isset(self::$_optionsDefaultCache[$storeId][$storeId][$_optionsAttribute['label']])) {
                self::$_optionsDefaultCache[$storeId][$_optionsAttribute['label']] = array();
            }
            self::$_optionsDefaultCache[$storeId][$_optionsAttribute['label']][] = $optionsDefaultTmp[$_optionsAttribute['value']];
        }

        foreach ($attributesIds as $attributeId) {
            if (!isset(self::$_optionsCache[$storeId][$attributeId]))
                self::$_optionsCache[$storeId][$attributeId] = array();
            if (!isset(self::$_optionsDefaultCache[$storeId][$attributeId]))
                self::$_optionsDefaultCache[$storeId][$attributeId] = array();
        }
    }

    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        /** @var $_hlp Evgick_Optimization_Helper_Data */
        $_hlp = Mage::helper('evgick_optimization');
        if (!$_hlp->rewriteLayer()) {
            return parent::getAllOptions($withEmpty, $defaultValues);
        }

        $storeId = $this->getAttribute()->getStoreId();
        if (!is_array($this->_options)) {
            $this->_options = array();
        }
        if (!is_array($this->_optionsDefault)) {
            $this->_optionsDefault = array();
        }
        $realStoreId = $storeId;
        if (is_null($storeId)) {
            $realStoreId = Mage::app()->getStore()->getId();
        }

        if (!isset($this->_options[$storeId]) && isset(self::$_optionsCache[$realStoreId]) && isset(self::$_optionsCache[$realStoreId][$this->getAttribute()->getId()])) {
            $this->_options[$storeId] = self::$_optionsCache[$realStoreId][$this->getAttribute()->getId()];
        }

        if (!isset($this->_optionsDefault[$storeId]) && isset(self::$_optionsDefaultCache[$realStoreId]) && isset(self::$_optionsDefaultCache[$realStoreId][$this->getAttribute()->getId()])) {
            $this->_optionsDefault[$storeId] = self::$_optionsDefaultCache[$realStoreId][$this->getAttribute()->getId()];
        }

        if (!isset($this->_options[$storeId])) {
            $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                ->setPositionOrder('asc')
                ->setAttributeFilter($this->getAttribute()->getId())
                ->setStoreFilter($this->getAttribute()->getStoreId())
                ->load();

            $this->_options[$storeId] = $collection->toOptionArray();
            $this->_optionsDefault[$storeId] = $collection->toOptionArray('default_value');
        }

        $options = ($defaultValues ? $this->_optionsDefault[$storeId] : $this->_options[$storeId]);

        if ($withEmpty) {
            array_unshift($options, array('label' => '', 'value' => ''));
        }

        return $options;
    }

    public function getOptionArray($withEmpty = true)
    {
        $_options = array();
        foreach ($this->getAllOptions($withEmpty) as $option) {
            $_options[$option['value']] = $option['label'];
        }
        return $_options;
    }
}
