<?php

/**
 * Url.php
 *
 * @category:   Evgick_Optimization
 * @apckage:    Evgick
 * @author:     Evgick <>
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Evgick_Optimization_Model_Indexer_Url extends Mage_Catalog_Model_Indexer_Url
{
    protected function _registerProductEvent(Mage_Index_Model_Event $event)
    {
        /** @var $_hlp Evgick_Optimization_Helper_Data */
        $_hlp = Mage::helper('evgick_optimization');
        if (!$_hlp->optimizeUrlRewrite()) {
            return parent::_registerProductEvent($event);
        }

        $product = $event->getDataObject();
        $dataChange2 = false;

        if (($product->dataHasChangedFor('status') && $product->getData('status') == "1") || ($product->dataHasChangedFor('visibility') && $product->getData('visibility') != "1")) {
            $dataChange2 = true;
        }

        $dataChange = $product->dataHasChangedFor('url_key')
            || $product->getIsChangedCategories()
            || $product->getIsChangedWebsites()
            || $dataChange2;

        if (!$product->getExcludeUrlRewrite() && $dataChange) {
            $event->addNewData('rewrite_product_ids', array($product->getId()));
        }
    }
}
