<?php

/**
 * Layer.php
 *
 * @category:   Evgick_Optimization
 * @apckage:    Evgick
 * @author:     Evgick <>
 */
class Evgick_Optimization_Model_Layer extends Mage_Catalog_Model_Layer
{
    /**
     * Get collection of all filterable attributes for layer products set
     *
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Attribute_Collection
     */
    public function getFilterableAttributes()
    {
        $collection = parent::getFilterableAttributes();

        /** @var $_hlp Evgick_Optimization_Helper_Data */
        $_hlp = Mage::helper('evgick_optimization');
        if ($_hlp->rewriteLayer() && count($collection)) {
            $attributes = array();
            foreach ($collection as $_attribute) {
                $attributes[] = $_attribute->getAttributeCode();
            }

            $_hlp->preloadProductAttributes($attributes, true);
        }

        return $collection;
    }
}
