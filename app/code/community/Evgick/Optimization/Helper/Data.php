<?php

/**
 * Data.php
 *
 * @category:   Evgick_Optimization
 * @apckage:    Evgick
 * @author:     Evgick <>
 */
class Evgick_Optimization_Helper_Data extends Mage_Core_Helper_Abstract
{
    const PATH_IS_ENABLE = 'evgick_optimization/optimization_group/active';
    const PATH_LAYOUT_UPDATE = 'evgick_optimization/optimization_group/layout_update';
    const PATH_LAYER = 'evgick_optimization/optimization_group/layer';
    const PATH_URL_REWRITE = 'evgick_optimization/optimization_group/url_rewrite';
    const PATH_CMS_BLOCKS_CACHE = 'evgick_optimization/optimization_group/cms_blocks_cache';

    private $mediaGallery = null;

    protected function _getStoreId()
    {
        return Mage::app()->getStore()->getId();
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return Mage::getStoreConfigFlag(self::PATH_IS_ENABLE, $this->_getStoreId());
    }

    /**
     * @return bool
     */
    protected function _isCmsBlocksCacheEnabled()
    {
        return Mage::app()->useCache('custom_cms_block');
    }

    /**
     * @return bool
     */
    protected function _isProductDetailCacheEnabled()
    {
        return Mage::app()->useCache('product_detail');
    }

    /**
     * @return bool
     */
    protected function _isCategoryListingCacheEnabled()
    {
        return Mage::app()->useCache('category_listing');
    }

    /**
     * @return bool
     */
    protected function _isLayoutUpdateEnabled()
    {
        return Mage::getStoreConfigFlag(self::PATH_LAYOUT_UPDATE, $this->_getStoreId());
    }

    /**
     * @return bool
     */
    protected function _isLayerEnabled()
    {
        return Mage::getStoreConfigFlag(self::PATH_LAYOUT_UPDATE, $this->_getStoreId());
    }

    /**
     * @return bool
     */
    protected function _isUrlRewriteEnabled()
    {
        return Mage::getStoreConfigFlag(self::PATH_URL_REWRITE, $this->_getStoreId());
    }

    /**
     * @return bool
     */
    public function cacheCmsBlocks()
    {
        return ($this->isEnabled() && $this->_isCmsBlocksCacheEnabled());
    }

    /**
     * @return bool
     */
    public function cacheProductDetails()
    {
        return ($this->isEnabled() && $this->_isProductDetailCacheEnabled());
    }

    /**
     * @return bool
     */
    public function cacheCategoryListingDetails()
    {
        return ($this->isEnabled() && $this->_isCategoryListingCacheEnabled());
    }


    /**
     * @return bool
     */
    public function rewriteLayoutUpdate()
    {
        return ($this->isEnabled() && $this->_isLayoutUpdateEnabled());
    }

    /**
     * @return bool
     */
    public function rewriteLayer()
    {
        return ($this->isEnabled() && $this->_isLayerEnabled());
    }

    /**
     * @return bool
     */
    public function optimizeUrlRewrite()
    {
        return ($this->isEnabled() && $this->_isUrlRewriteEnabled());
    }

//    public function getListImageData(Mage_Catalog_Model_Product $product, $file = null)
//    {
//        $imageTypes = array('small_image', 'alt_small_image');
//        $imageHl = Mage::helper('catalog/image');
//        $data = '';
//        if ($file) {
//            $data .= ' data-' . $imageTypes[0] . '="' . $imageHl->init($product, '', $file)->resize(368, 523) . '"';
//        } else {
//            foreach ($imageTypes as $imageType) {
//                $data .= ' data-' . $imageType . '="' . $imageHl->init($product, $imageType)->resize(368, 523) . '"';
//            }
//        }
//        return $data;
//    }

    public function preloadCategoryAttributes($attributes)
    {
        Mage::getSingleton('eav/config')->preloadAttributes(Mage_Catalog_Model_Category::ENTITY, $attributes);
    }

    public function preloadCustomerAttributes($attributes, $withOptions = false)
    {
        Mage::getSingleton('eav/config')->preloadAttributes('customer', $attributes);
        if ($withOptions) {
            Mage::getSingleton('eav/entity_attribute_source_table')->preloadOptions('customer', $attributes, Mage::app()->getStore()->getId());
        }
    }

    public function preloadProductAttributes($attributes, $withOptions = false)
    {
        Mage::getSingleton('eav/config')->preloadAttributes(Mage_Catalog_Model_Product::ENTITY, $attributes);
        if ($withOptions) {
            Mage::getSingleton('eav/entity_attribute_source_table')->preloadOptions(Mage_Catalog_Model_Product::ENTITY, $attributes, Mage::app()->getStore()->getId());
        }
    }

    public function addMediaGalleryAttributeToProductArray(array $_productArray)
    {
        $_mediaGalleryAttributeId = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'media_gallery')->getAttributeId();
        $_read = Mage::getSingleton('core/resource')->getConnection('catalog_read');
        $tableMG = Mage::getSingleton('core/resource')->getTableName('catalog_product_entity_media_gallery');
        $tableMGV = Mage::getSingleton('core/resource')->getTableName('catalog_product_entity_media_gallery_value');

        $_mediaGalleryData = $_read->fetchAll('
            SELECT
                main.entity_id, `main`.`value_id`, `main`.`value` AS `file`,
                `value`.`label`, `value`.`position`, `value`.`disabled`, `default_value`.`label` AS `label_default`,
                `default_value`.`position` AS `position_default`,
                `default_value`.`disabled` AS `disabled_default`
            FROM `' . $tableMG . '` AS `main`
                LEFT JOIN `' . $tableMGV . '` AS `value`
                    ON main.value_id=value.value_id AND value.store_id=' . Mage::app()->getStore()->getId() . '
                LEFT JOIN `' . $tableMGV . '` AS `default_value`
                    ON main.value_id=default_value.value_id AND default_value.store_id=0
            WHERE (
                main.attribute_id = ' . $_read->quote($_mediaGalleryAttributeId) . ')
                AND (main.entity_id IN (' . $_read->quote(array_keys($_productArray)) . '))
            ORDER BY IF(value.position IS NULL, default_value.position, value.position) ASC
        ');

        $_mediaGalleryByProductId = array();
        foreach ($_mediaGalleryData as $_galleryImage) {
            $k = $_galleryImage['entity_id'];
            unset($_galleryImage['entity_id']);
            if (!isset($_mediaGalleryByProductId[$k])) {
                $_mediaGalleryByProductId[$k] = array();
            }
            $_mediaGalleryByProductId[$k][] = $_galleryImage;
        }
        unset($_mediaGalleryData);
        foreach ($_productArray as &$_product) {
            $_productId = $_product->getData('entity_id');
            if (isset($_mediaGalleryByProductId[$_productId])) {
                $_product->setData('media_gallery', $_mediaGalleryByProductId[$_productId]);
            }
        }
        unset($_mediaGalleryByProductId);

        return $_productArray;
    }

    public function addMediaGalleryAttributeToProductCollection(Mage_Catalog_Model_Resource_Product_Collection $_productCollection, $colorFilter = array())
    {
        $_mediaGalleryAttributeId = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'media_gallery')->getAttributeId();
        $_read = Mage::getSingleton('core/resource')->getConnection('catalog_read');
        $tableMG = Mage::getSingleton('core/resource')->getTableName('catalog_product_entity_media_gallery');
        $tableMGV = Mage::getSingleton('core/resource')->getTableName('catalog_product_entity_media_gallery_value');

        $colorFilterQuery = '';
        if (count($colorFilter)) {
            $colorFilterQuery = 'AND `default_value`.`color` IN (' . $_read->quote($colorFilter) . ')';
        }

        $_mediaGalleryData = $_read->fetchAll('
            SELECT
                main.entity_id, `main`.`value_id`, `main`.`value` AS `file`,
                `value`.`label`,
                `value`.`position`,
                `value`.`color`,
                `value`.`disabled`,
                `default_value`.`label` AS `label_default`,
                `default_value`.`position` AS `position_default`,
                `default_value`.`color` AS `color_default`,
                `default_value`.`disabled` AS `disabled_default`
            FROM `' . $tableMG . '` AS `main`
                LEFT JOIN `' . $tableMGV . '` AS `value`
                    ON main.value_id=value.value_id AND value.store_id=' . Mage::app()->getStore()->getId() . '
                LEFT JOIN `' . $tableMGV . '` AS `default_value`
                    ON main.value_id=default_value.value_id AND default_value.store_id=0
            WHERE (
                main.attribute_id = ' . $_read->quote($_mediaGalleryAttributeId) . ')
                ' . $colorFilterQuery . '
                AND (main.entity_id IN (' . $_read->quote($_productCollection->getAllIds()) . '))
            ORDER BY IF(value.position IS NULL, default_value.position, value.position) ASC
        ');

        $_mediaGalleryByProductId = array();
        foreach ($_mediaGalleryData as $_galleryImage) {
            $k = $_galleryImage['entity_id'];
            unset($_galleryImage['entity_id']);
            if (!isset($_mediaGalleryByProductId[$k])) {
                $_mediaGalleryByProductId[$k] = array();
            }
            $_mediaGalleryByProductId[$k][] = $_galleryImage;
        }
        unset($_mediaGalleryData);
        foreach ($_productCollection as &$_product) {
            $_productId = $_product->getData('entity_id');
            if (isset($_mediaGalleryByProductId[$_productId])) {
                $_product->setData('media_gallery', array('images' => $_mediaGalleryByProductId[$_productId]));
            }
        }
        unset($_mediaGalleryByProductId);

        return $_productCollection;
    }

    public function getConfigurableGalleryImagesFromChildren($product)
    {
        if (is_null($this->mediaGallery) || !isset($this->mediaGallery[$product->getId()])) {
            if (!is_array($this->mediaGallery)) {
                $this->mediaGallery = array();
            }

            $mediaGallery = array();
            $allProducts = $product->getTypeInstance(true)
                ->getUsedProducts(null, $product);

            $productArray = array();
            foreach ($allProducts as $_childProduct) {
                $productArray[$_childProduct->getId()] = $_childProduct;
            }

            $productArray = $this->addMediaGalleryAttributeToProductArray($productArray);

            foreach ($productArray as $product) {
                if ($_productMediaGallery = $product->getMediaGallery()) {
                    foreach ($_productMediaGallery as $_productImage) {
                        $image = new Varien_Object();
                        //var_dump($_productImage);die;
                        $image->setData($_productImage);
                        $image->setColor($product->getColor());
                        $mediaGallery[] = $image;
                    }
                }
            }

            $this->mediaGallery[$product->getId()] = $mediaGallery;
        }

        return $this->mediaGallery[$product->getId()];
    }

    public function appendOptionsToProductCollection(Mage_Catalog_Model_Resource_Product_Collection &$_productCollection, array $attributes)
    {
        $productIds = $_productCollection->getAllIds();

        $collection = Mage::getResourceModel('catalog/product_type_configurable_product_collection')
            ->addAttributeToSelect($attributes);

        $collection->getSelect()->where('link_table.parent_id in(?)', $productIds);

        $options = array();
        foreach ($collection as $item) {
            if (!isset($options[$item->getParentId()])) {
                $options[$item->getParentId()] = array();
            }
            foreach ($attributes as $attributeCode) {
                if (!isset($options[$item->getParentId()][$attributeCode])) {
                    $options[$item->getParentId()][$attributeCode] = array();
                }
                if (!isset($options[$item->getParentId()][$attributeCode][$item->getData($attributeCode)])) {
                    $options[$item->getParentId()][$attributeCode][$item->getData($attributeCode)] = $item->getData($attributeCode);
                }
            }
        }

        foreach ($_productCollection as &$_product) {
            if (isset($options[$_product->getId()])) {
                $_product->setPreloadedOptions($options[$_product->getId()]);
            }
        }

        return $this;
    }

    public function appendOptionsToProduct(Mage_Catalog_Model_Product &$_product, array $attributes)
    {

        $collection = $_product->getTypeInstance(true)
            ->getUsedProducts(null, $_product);

        $options = array();
        foreach ($collection as $item) {
            if ($item->getStatus() == 1) {
                if (!isset($options[$item->getParentId()])) {
                    $options[$item->getParentId()] = array();
                }
                foreach ($attributes as $attributeCode) {
                    if (!isset($options[$item->getParentId()][$attributeCode])) {
                        $options[$item->getParentId()][$attributeCode] = array();
                    }
                    if (!isset($options[$item->getParentId()][$attributeCode][$item->getData($attributeCode)])) {
                        $options[$item->getParentId()][$attributeCode][$item->getData($attributeCode)] = $item->getData($attributeCode);
                    }
                }
            }
        }

        $_product->setPreloadedOptions($options[$_product->getId()]);

        return $this;
    }
}
